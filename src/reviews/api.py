from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import Review, Company
from .serializers import ReviewSerializer, CompanySerializer
from django.shortcuts import get_object_or_404


class ReviewsApi(APIView):
    def get(self, request, format=None):
        "returns the reviews of the user"
        objects = Review.objects.filter(reviewer=request.user)
        serializer = ReviewSerializer(objects, many=True)
        return Response({'objects': serializer.data})

    def post(self, request, format=None):
        "creates a new review"
        serializer = ReviewSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        review = Review(**serializer.validated_data)
        review.reviewer = request.user
        review.ip_address = request.META.get('HTTP_X_FORWARDED_FOR', '') or request.META.get('REMOTE_ADDR')
        try:
            review.company
        except Company.DoesNotExist:
            return Response({'company_id': ['Invalid Company ID']}, status=status.HTTP_400_BAD_REQUEST)
        review.save()
        serializer = ReviewSerializer(review)
        return Response(serializer.data)


class ReviewDetailApi(APIView):
    def get(self, request, pk, format=None):
        review = get_object_or_404(Review, pk=pk, reviewer=request.user)
        serializer = ReviewSerializer(review)
        return Response(serializer.data)


class CompaniesApi(APIView):
    def get(self, request, format=None):
        "returns the reviews of the user"
        objects = Company.objects.all()
        serializer = CompanySerializer(objects, many=True)
        return Response({'objects': serializer.data})

