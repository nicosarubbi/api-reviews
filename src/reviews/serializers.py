from django.contrib.auth.models import User
from rest_framework import serializers
from .models import Review, Company


class ReviewerSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'email')


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ('id', 'name', 'description')


class ReviewSerializer(serializers.ModelSerializer):
    reviewer = ReviewerSerializer(read_only=True)
    company = CompanySerializer(read_only=True)
    company_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = Review
        fields= ('id', 'reviewer', 'rating', 'title', 'summary', 'company_id',
                 'company','ip_address', 'submission_date')
        read_only_fields = ('id', 'reviewer', 'company', 'ip_address', 'submission_date')

