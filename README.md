Installation (Python3)
============

```bash
mkvirtualenv --python=/usr/bin/python3.5 baires
pip install -r requirements.txt
```

```bash
cd src
./manage.py createdb
./manage.py migrate
./manage.py runserver
```

Documentation [Here](https://docs.google.com/document/d/1WDkdG7JhGT82Pt0F1ivoT7SFxT6gRVR0lqbbQODuVh0/edit?usp=sharing)

How to test:

- After running the migrations, load the test fixture to create some data for testing.
```
./manage.py loaddata test_fixture.json
```

- There is an admin user called "admin", with password "qwer1234"

- All the users in the fixture file have the password set as 'qwer1234'.

- The admin page is in http://<host>/admin/

- The API is described in the documentation above