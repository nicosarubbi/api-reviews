from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token


class Review(models.Model):
    RATING_CHOICES = [(x,x) for x in range(1,6)]
    reviewer = models.ForeignKey(User, related_name="reviews",
                                 on_delete=models.PROTECT)
    rating = models.IntegerField(choices=RATING_CHOICES)
    title = models.CharField(max_length=64)
    summary = models.TextField(max_length=10000)
    ip_address = models.GenericIPAddressField()
    submission_date = models.DateTimeField(auto_now_add=True)
    company = models.ForeignKey("Company", related_name="reviews",
                                on_delete=models.PROTECT)

    def __str__(self):
        return "[%i] %s -- %s" % (self.rating, self.company,
                                  self.reviewer.get_full_name())


class Company(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField()

    def __str__(self):
        return self.name


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)

