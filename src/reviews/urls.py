from django.conf.urls import url, include
from . import api

urlpatterns = [
    url(r"^reviews/$", api.ReviewsApi.as_view(), name="Reviews"),
    url(r"^reviews/(?P<pk>[0-9]+)/$", api.ReviewDetailApi.as_view(), name='reviewDetail'),
    url(r"^companies/$", api.CompaniesApi.as_view(), name='companies'),
]

