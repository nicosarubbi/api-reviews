from django.contrib import admin
from .models import Review, Company
from django.contrib.auth.models import User


class ReviewAdmin(admin.ModelAdmin):
    list_display = ("company", "stars", "title", "reviewer_name", "submission_date",)

    def reviewer_name(self, obj):
        return obj.reviewer.get_full_name()
    reviewer_name.short_description = "Reviewer"

    def stars(self, obj):
        return '*' * obj.rating
    stars.short_description = "Rating"


class CompanyAdmin(admin.ModelAdmin):
    list_display = ('name', 'description')


admin.site.register(Review, ReviewAdmin)
admin.site.register(Company, CompanyAdmin)
